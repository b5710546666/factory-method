#Factory Pattern#


Factory pattern is one of most used design pattern in Java. This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.
In Factory pattern, we create object without exposing the creation logic to the client and refer to newly created object using a common interface.


**For example:**

We’re going to create a each of Shape by using Factory method pattern first this we need is a Shape interface and the concrete classes that implemented Shape interface

**Step 1:** create the interface of our class


```
#!java
public interface Shape {
   void draw();
}
```


This is the Shape interface, method draw can be anything we want. Maybe getRadius(), getVolume() or something


**Step 2:** create concrete classes


```
#!java

public class Rectangle implements Shape {

   @Override
   public void draw() {
      System.out.println("Inside Rectangle::draw() method.");
   }
}
public class Square implements Shape {

   @Override
   public void draw() {
      System.out.println("Inside Square::draw() method.");
   }
}
public class Circle implements Shape {

   @Override
   public void draw() {
      System.out.println("Inside Circle::draw() method.");
   }
}

```

**Step 3:** create Factory class for our Shape


```
#!java

public class ShapeFactory {
	
   //use getShape method to get object of type shape 
   public Shape getShape(String shapeType){
      if(shapeType == null){
         return null;
      }		
      if(shapeType.equalsIgnoreCase("CIRCLE")){
         return new Circle();
         
      } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
         return new Rectangle();
         
      } else if(shapeType.equalsIgnoreCase("SQUARE")){
         return new Square();
      }
      
      return null;
   }
}

```

**Step 4:** create main Class to call factory method


```
#!java

public class FactoryPatternDemo {

   public static void main(String[] args) {
      ShapeFactory shapeFactory = new ShapeFactory();

      //get an object of Circle and call its draw method.
      Shape shape1 = shapeFactory.getShape("CIRCLE");

      //call draw method of Circle
      shape1.draw();

      //get an object of Rectangle and call its draw method.
      Shape shape2 = shapeFactory.getShape("RECTANGLE");

      //call draw method of Rectangle
      shape2.draw();

      //get an object of Square and call its draw method.
      Shape shape3 = shapeFactory.getShape("SQUARE");

      //call draw method of circle
      shape3.draw();
   }
}
```


The out put will be like this


```
#!java

Inside Circle::draw() method.

Inside Rectangle::draw() method.

Inside Square::draw() method.
```



![11198824_10204004246602984_2033791226_n.jpg](https://bitbucket.org/repo/zyragR/images/1710205749-11198824_10204004246602984_2033791226_n.jpg)